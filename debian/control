Source: seqan2
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Michael R. Crusoe <crusoe@debian.org>,
           Kevin Murray <spam@kdmurray.id.au>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               python3,
               cmake,
               fakeroot
Build-Depends-Arch: zlib1g-dev,
                    libbz2-dev,
                    libbam-dev,
                    libboost-dev,
                    help2man,
                    ctdconverter
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/seqan2
Vcs-Git: https://salsa.debian.org/med-team/seqan2.git
Homepage: https://www.seqan.de/
Rules-Requires-Root: no

Package: seqan-apps
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: cwltool | cwl-runner
Description: C++ library for the analysis of biological sequences
 SeqAn is a C++ template library of efficient algorithms and data
 structures for the analysis of sequences with the focus on
 biological data. This library applies a unique generic design that
 guarantees high performance, generality, extensibility, and
 integration with other libraries. SeqAn is easy to use and
 simplifies the development of new software tools with a minimal loss
 of performance. This package contains the applications dfi, pair_align,
 micro_razers, seqan_tcoffee, seqcons, razers and tree_recon.

Package: libseqan2-dev
Architecture: all
Multi-Arch: foreign
Section: libdevel
Depends: ${misc:Depends}
Description: C++ library for the analysis of biological sequences (development)
 SeqAn is a C++ template library of efficient algorithms and data
 structures for the analysis of sequences with the focus on
 biological data. This library applies a unique generic design that
 guarantees high performance, generality, extensibility, and
 integration with other libraries. SeqAn is easy to use and
 simplifies the development of new software tools with a minimal loss
 of performance.
 .
 This package contains the developer files.
